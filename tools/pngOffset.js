#!/usr/bin/nodejs
const extract = require('png-chunks-extract')
const encode = require('png-chunks-encode')
const path = require('path')
const fs = require('fs')
const process = require('process')

// File:
if (!process.argv[2]) {
    throw new Error("Missing filepath. Useage: 'pngOffset filepath x y'. Offset may also be 'monster' or 'projectile' in place of x and y for auto offsets.")
}
const filepath = path.join(__dirname, process.argv[2])
const filename = filepath

// Read:
const buffer = fs.readFileSync(filepath)
const chunks = extract(buffer)
const width = chunks.find(chunk => chunk.name == 'IHDR').data[3]
const height = chunks.find(chunk => chunk.name == 'IHDR').data[7]

// Offsets
const offsets = {
    x: process.argv[3],
    y: process.argv[4],
}
if (process.argv[3] === 'monster') {
    offsets.x = Math.round(width / 2)
    offsets.y = height - 4
} else if (process.argv[3] === 'projectile') {
    offsets.x = Math.round(width / 2)
    offsets.y = Math.round(height / 2)
}
if (offsets.x === undefined || offsets.y === undefined) {
    throw new Error("Missing or invalid offsets. Provide either x and y offsets, 'monster' or 'projectile'.")
}

// Grab:
let grAb = chunks.find(chunk => chunk.name == 'grAb')
if (!grAb) {
    grAb = {
        name: 'grAb',
        data: [
            0, 0, 0, 0,
            0, 0, 0, 0,
        ],
    }
    chunks.splice(1, 0, grAb)
}

// Write:
grAb.data[3] = offsets.x
grAb.data[7] = offsets.y
fs.writeFileSync(filepath, Buffer.from(encode(chunks)))
console.log("Set offset of " + filename + " to " + offsets.x + " x " + offsets.y)
