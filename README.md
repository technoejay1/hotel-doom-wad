# Hotel Doom Wad

## Requirements
GZDoom and doom 2 iwad 

## Run  
```
gzdoom -file src
```


## Build
```
mkdir -p bin
zip -r bin/hotel_doom.pk3 src/*
gzdoom -file bin/hotel_doom.pk3
```
## Credits
- Created by Connor and Kris Occhipinti
- Civilian Sprites by DeeDeeOZ https://forum.zdoom.org/viewtopic.php?t=72748


//male goon/hoodlum/mook/thug/roider/ninja
these are built on the male base. Credits Mark Quinn,Skelegant,Skulltag head
shotgun flash is from Project Brutality.

//mobster 1/2
Credits Captain J,Scalliano,Skelegant
Revolver from DirtyHarry skin by Mike12

//russian Ak mobster
Credits Captain J,Scalliano,Skelegant
Ak47 from James Bond skulltag skin by d00ds from Goldeneye TC
Unknown Head?

//bandit gunman
this is built on the female base. Credits to DavidRaven,Mryayayify
Ak47 from James Bond skulltag skin by d00ds from Goldeneye TC

//heavy body bigguy/Big Merc
this is built on the doom2 commando specifically the unamred version/rifle version
credits to David G (ItsNatureToDie),JoeyTD and TommyGalano5

//merc trooper + gasmask Merc
this is good old doomguy,with skulltag head
Credits to Id Software,Matt,Solo_Spaghetti and TommyGalano5

//Old school mobsters
Recoloured version of Orka's shotgun mobster,Captain J,Scalliano
Tommy gun is from Orka's Mr.X guy.

//sounds
see/pain/death sounds from Soldier of Fortune 1,Raven Software
surrender sounds from Fallout 3 enclave male, Bethesda Softworks
